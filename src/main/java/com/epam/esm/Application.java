package com.epam.esm;

import java.io.File;
import org.apache.catalina.startup.Tomcat;

public class Application {

    public static void main(String[] args) throws Exception {

        String webAppDirLocation = "src/main/";
        Tomcat tomcat = new Tomcat();

        tomcat.setPort(8080);
        tomcat.getConnector();
        tomcat.addWebapp("", new File(webAppDirLocation).getAbsolutePath());
        tomcat.start();
        tomcat.getServer()
              .await();
    }
}
