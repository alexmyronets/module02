package com.epam.esm.data.impl;

import static com.epam.esm.data.DBConstants.SQL_DELETE_TAG;
import static com.epam.esm.data.DBConstants.SQL_INSERT_TAG;
import static com.epam.esm.data.DBConstants.SQL_QUERY_ALL_TAGS;
import static com.epam.esm.data.DBConstants.SQL_QUERY_COUNT_CERTIFICATES_BY_TAG;
import static com.epam.esm.data.DBConstants.SQL_QUERY_TAG;
import static com.epam.esm.data.DBConstants.SQL_QUERY_TAG_BY_NAME;

import com.epam.esm.data.TagRepo;
import com.epam.esm.domain.Tag;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class TagRepoImpl implements TagRepo {

    private static final Logger LOGGER = LogManager.getLogger(TagRepoImpl.class);

    private final JdbcTemplate jdbcTemplate;

    public TagRepoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Tag create(Tag tag) {
        LOGGER.info("Saving tag {} into DB", tag);
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        Long tagId;

        if (isTagExists(tag.getName())) {
            LOGGER.debug("Tag with name {}, already exists", tag::getName);
            throw new TagAlreadyExistsException("Tag (name = " + tag.getName() + ") is already exists");
        }
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(SQL_INSERT_TAG, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, tag.getName());
            return ps;
        }, keyHolder);
        tagId = (Long) Objects.requireNonNull(keyHolder.getKeys())
                              .get("id");
        LOGGER.debug("Generated Tag id is: {}", tagId);
        LOGGER.info("Returning newly created tag");
        return read(tagId);
    }

    @Override
    public Tag read(long id) {
        LOGGER.info("Querying Tag by id = {} from DB", id);
        List<Tag> tagList = jdbcTemplate.query(SQL_QUERY_TAG, this::mapRowToTag, id);
        if (tagList.isEmpty()) {
            LOGGER.debug("Tag with id = {} doesn't exist", id);
            throw new NoSuchTagException("Requested resource not found (id = " + id + ")");
        }
        LOGGER.info("Returning requested Tag with id = {}", id);
        return tagList.get(0);
    }

    @Override
    public void delete(long id) {
        LOGGER.info("Removing Tag with id = {} from DB", id);
        int certCount = countCertificatesByTag(id);
        if (certCount > 0) {
            LOGGER.debug("Tag with id = {} cannot be deleted, because it in use by {} Gift Certificates", id, certCount);
            throw new TagIsInUseException("Tag (id = " + id + ") is in use by " + certCount + " gift certificates");
        }
        if (jdbcTemplate.update(SQL_DELETE_TAG, id) == 0) {
            LOGGER.debug("Tag with id = {} doesn't exist", id);
            throw new NoSuchTagException("Requested resource not found (id = " + id + ")");
        }
    }

    @Override
    public List<Tag> list() {
        LOGGER.info("Querying and returning List of all tags from DB");
        return jdbcTemplate.query(SQL_QUERY_ALL_TAGS, this::mapRowToTag);
    }

    private int countCertificatesByTag(long id) {
        LOGGER.debug("Counting amount of Gift Certificates for Tag with id = {}", id);
        return Objects.requireNonNull(jdbcTemplate.queryForObject(SQL_QUERY_COUNT_CERTIFICATES_BY_TAG, Integer.class, id));
    }

    private boolean isTagExists(String name) {
        LOGGER.debug("Querying whether Tag with name = {} exists", name);
        List<Tag> tags = Objects.requireNonNull(jdbcTemplate.query(SQL_QUERY_TAG_BY_NAME, this::mapRowToTag, name));
        return !tags.isEmpty();
    }
}
