package com.epam.esm.data.impl;

import static com.epam.esm.data.DBConstants.SQL_DELETE_GIFT_CERTIFICATE;
import static com.epam.esm.data.DBConstants.SQL_DELETE_GIFT_CERTIFICATE_TAG_REFERENCE;
import static com.epam.esm.data.DBConstants.SQL_INSERT_GIFT_CERTIFICATE;
import static com.epam.esm.data.DBConstants.SQL_INSERT_GIFT_CERTIFICATE_TAG_REFERENCE;
import static com.epam.esm.data.DBConstants.SQL_QUERY_ALL_GIFT_CERTIFICATES;
import static com.epam.esm.data.DBConstants.SQL_QUERY_GIFT_CERTIFICATE;
import static com.epam.esm.data.DBConstants.SQL_QUERY_TAGS_BY_GIFT_CERTIFICATE;
import static com.epam.esm.data.DBConstants.SQL_UPDATE_GIFT_CERTIFICATE;

import com.epam.esm.data.GiftCertRepo;
import com.epam.esm.data.TagRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.Tag;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class GiftCertRepoImpl implements GiftCertRepo {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertRepoImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final TagRepo tagRepo;

    public GiftCertRepoImpl(JdbcTemplate jdbcTemplate, TagRepo tagRepo) {
        this.jdbcTemplate = jdbcTemplate;
        this.tagRepo = tagRepo;
    }

    @Override
    @Transactional
    public GiftCertificate create(GiftCertificate giftCert) {
        LOGGER.info("Saving Gift Certificate {} into DB", giftCert);
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        Long certId;
        LocalDateTime createAndLastUpdateTime = LocalDateTime.now();
        LOGGER.debug("Create time is {}", createAndLastUpdateTime);
        jdbcTemplate.update(con -> {
            PreparedStatement ps = con.prepareStatement(SQL_INSERT_GIFT_CERTIFICATE, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, giftCert.getName());
            ps.setString(2, giftCert.getDescription());
            ps.setBigDecimal(3, giftCert.getPrice());
            ps.setShort(4, giftCert.getDuration());
            ps.setObject(5, createAndLastUpdateTime);
            ps.setObject(6, createAndLastUpdateTime);
            return ps;
        }, keyHolder);
        certId = (Long) Objects.requireNonNull(keyHolder.getKeys())
                               .get("id");
        LOGGER.debug("Generated id is {}", certId);
        createGiftCertificateTags(giftCert.getTags());
        saveGiftCertificateTagsRefs(certId, giftCert.getTags());
        LOGGER.info("Returning just created Gift Certificate");
        return read(certId);
    }

    @Override
    public GiftCertificate read(long id) {
        LOGGER.info("Querying Gift Certificate by id = {} from DB", id);
        List<GiftCertificate> giftCertList = jdbcTemplate.query(SQL_QUERY_GIFT_CERTIFICATE, this::mapRowToGiftCertificate, id);
        List<Tag> tags;
        GiftCertificate giftCert;

        if (giftCertList.isEmpty()) {
            LOGGER.debug("Gift Certificate with id = {} doesn't exist", id);
            throw new NoSuchGiftCertificateException("Requested resource not found (id = " + id + ")");
        }
        giftCert = giftCertList.get(0);
        tags = getGiftCertTags(id);
        giftCert.setTags(tags);
        LOGGER.info("Returning requested Gift Certificate with id = {}", id);
        return giftCert;
    }

    @Override
    @Transactional
    public GiftCertificate update(GiftCertificate patch, long id) {
        LOGGER.info("Updating DB record of Gift Certificate with id = {}", id);
        GiftCertificate current = read(id);

        setPatchValues(current, patch);
        current.setLastUpdateDate(LocalDateTime.now());
        jdbcTemplate.update(SQL_UPDATE_GIFT_CERTIFICATE,
                            new Object[]{current.getName(), current.getDescription(), current.getPrice(), current.getDuration(),
                                current.getLastUpdateDate(), id},
                            new int[]{Types.VARCHAR, Types.VARCHAR, Types.DECIMAL, Types.SMALLINT, Types.TIMESTAMP, Types.BIGINT});

        if (patch.getTags() == null) {
            LOGGER.info("No update for Tags, Tags will remain the same");
            return read(id);
        }

        updateTags(patch, id, current);
        return read(id);
    }


    @Override
    public void delete(long id) {
        LOGGER.info("Removing Gift Certificate with id = {} from DB", id);
        if (jdbcTemplate.update(SQL_DELETE_GIFT_CERTIFICATE, id) == 0) {
            LOGGER.debug("Gift Certificate with id = {} doesn't exist", id);
            throw new NoSuchGiftCertificateException("Requested resource not found (id = " + id + ")");
        }
    }

    @Override
    public List<GiftCertificate> list() {
        LOGGER.info("Querying List of all Gift Certificates from DB");
        List<GiftCertificate> giftCerts = jdbcTemplate.query(SQL_QUERY_ALL_GIFT_CERTIFICATES, this::mapRowToGiftCertificate);
        giftCerts.forEach(giftCert -> giftCert.setTags(getGiftCertTags(giftCert.getId())));
        LOGGER.info("Returning List of all Gift Certificates");
        return giftCerts;
    }

    private void updateTags(GiftCertificate patch, long id, GiftCertificate current) {
        LOGGER.info("Updating Tags for Gift Certificate id = {}", id);
        List<Tag> tagsToDelete;
        List<Tag> patchTags;
        List<Tag> currentTags;
        List<Tag> tagsToAdd;

        currentTags = current.getTags();
        patchTags = patch.getTags();
        tagsToDelete = currentTags.stream()
                                  .filter(tag -> !patchTags.contains(tag))
                                  .toList();
        tagsToAdd = patchTags.stream()
                             .filter(tag -> !currentTags.contains(tag))
                             .toList();
        LOGGER.debug("Creating tags from Gift Certificate patch if Tag does not exist");
        createGiftCertificateTags(patchTags);
        LOGGER.debug("Removing Tags references if Tag absent in Gift Certificate patch");
        deleteTagsRefs(id, tagsToDelete);
        LOGGER.debug("Adding Tag references for new Tag from Gift Certificate patch");
        saveGiftCertificateTagsRefs(id, tagsToAdd);
    }

    private void setPatchValues(GiftCertificate current, GiftCertificate patch) {
        LOGGER.debug("Setting new values from the patch");
        if (patch.getName() != null) {
            LOGGER.debug("New name: {}", patch::getName);
            current.setName(patch.getName());
        }
        if (patch.getDescription() != null) {
            LOGGER.debug("New description: {}", patch::getDescription);
            current.setDescription(patch.getDescription());
        }
        if (patch.getPrice() != null) {
            LOGGER.debug("New price: {}", patch::getPrice);
            current.setPrice(patch.getPrice());
        }
        if (patch.getDuration() != null) {
            LOGGER.debug("New duration: {}", patch::getDuration);
            current.setDuration(patch.getDuration());
        }
    }

    private List<Tag> getGiftCertTags(long id) {
        LOGGER.info("Querying Tags list for Gift Certificate with id = {}", id);
        return jdbcTemplate.query(SQL_QUERY_TAGS_BY_GIFT_CERTIFICATE, tagRepo::mapRowToTag, id);
    }

    private void createGiftCertificateTags(List<Tag> tags) {
        LOGGER.info("Saving new Tags for Gift Certificate into DB");
        tags.forEach(tag -> {
            if (tag.getId() == null) {
                LOGGER.debug("Tag with name = {} need to be saved in DB", tag::getName);
                tag.setId(tagRepo.create(tag)
                                 .getId());
            }
        });
    }

    private void saveGiftCertificateTagsRefs(Long certId, List<Tag> tags) {
        LOGGER.info("Saving references for Gift Certificate id = {} Tags into DB", certId);
        jdbcTemplate.batchUpdate(SQL_INSERT_GIFT_CERTIFICATE_TAG_REFERENCE, tags, tags.size(), (ps, tag) -> {
            ps.setLong(1, certId);
            ps.setLong(2, tag.getId());
        });
    }

    private void deleteTagsRefs(Long certId, List<Tag> tags) {
        LOGGER.info("Removing references for Gift Certificate id = {} Tags into DB", certId);
        jdbcTemplate.batchUpdate(SQL_DELETE_GIFT_CERTIFICATE_TAG_REFERENCE, tags, tags.size(), (ps, tag) -> {
            ps.setLong(1, certId);
            ps.setLong(2, tag.getId());
        });
    }

    private GiftCertificate mapRowToGiftCertificate(ResultSet rs, int rowNum) throws SQLException {
        LOGGER.debug("Mapping Result Set to Gift Certificate object");
        GiftCertificate giftCert = new GiftCertificate();

        giftCert.setId(rs.getLong("id"));
        giftCert.setName(rs.getString("name"));
        giftCert.setDescription(rs.getString("description"));
        giftCert.setPrice(rs.getBigDecimal("price"));
        giftCert.setDuration(rs.getShort("duration"));
        giftCert.setCreateDate(rs.getObject("create_date", LocalDateTime.class));
        giftCert.setLastUpdateDate(rs.getObject("last_update_date", LocalDateTime.class));
        return giftCert;
    }
}
