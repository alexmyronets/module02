package com.epam.esm.data.impl;

import static com.epam.esm.data.DBConstants.GROUP_BY;
import static com.epam.esm.data.DBConstants.ORDER_BY;
import static com.epam.esm.data.DBConstants.SQL_QUERY_SEARCH_TEMPLATE;

import com.epam.esm.data.GiftCertRepo;
import com.epam.esm.data.SearchRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.SearchParametersDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SearchRepoImpl implements SearchRepo {

    private static final Logger LOGGER = LogManager.getLogger(SearchRepoImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final GiftCertRepo giftCertRepo;

    public SearchRepoImpl(JdbcTemplate jdbcTemplate, GiftCertRepo giftCertRepo) {
        this.jdbcTemplate = jdbcTemplate;
        this.giftCertRepo = giftCertRepo;
    }

    public List<GiftCertificate> readGiftCertificatesBySearchParams(SearchParametersDTO searchParams) {
        LOGGER.info("Quering DB for certificate by search parameters: {}", searchParams);
        StringBuilder searchQuery = new StringBuilder(SQL_QUERY_SEARCH_TEMPLATE);
        List<Long> giftCertIds;
        Object[] queryArgs;

        composeSearchQuery(searchQuery, searchParams);
        queryArgs = composeSearchParamsArray(searchParams);
        giftCertIds = jdbcTemplate.query(searchQuery.toString(), this::mapRowToGiftCertificateId, queryArgs);

        return giftCertIds.stream()
                          .map(giftCertRepo::read)
                          .toList();
    }

    private Long mapRowToGiftCertificateId(ResultSet rs, int rowNum) throws SQLException {
        return rs.getLong("id");
    }

    private void composeSearchQuery(StringBuilder searchQuery, SearchParametersDTO searchParams) {
        LOGGER.debug("Composing search query");
        if (searchParams.getTag() != null) {
            LOGGER.debug("Adding Tag name to search query");
            addTagNameToSearch(searchQuery);
        }
        if (searchParams.getSearchText() != null) {
            LOGGER.debug("Adding searching text to search query");
            addTextToSearch(searchQuery);
        }
        if (searchParams.getNameSearchOrder() != null) {
            LOGGER.debug("Adding sort order for Gift Certificate Name to search query: {}", searchParams::getNameSearchOrder);
            addSortColumnToSearch(searchQuery,
                                  "gift_certificate.name",
                                  searchParams.getNameSearchOrder()
                                              .name());
        }
        if (searchParams.getDateSearchOrder() != null) {
            LOGGER.debug("Adding sort order for Gift Certificate creation date to search query: {}", searchParams::getDateSearchOrder);
            addSortColumnToSearch(searchQuery,
                                  "gift_certificate.create_date",
                                  searchParams.getDateSearchOrder()
                                              .name());
        }
        LOGGER.debug("Composed search query: {}", searchQuery);
    }

    private Object[] composeSearchParamsArray(SearchParametersDTO searchParams) {
        LOGGER.debug("Composing array of parameters for insertion into prepared statement");
        List<String> argsList = new ArrayList<>();

        if (searchParams.getTag() != null) {
            LOGGER.debug("Adding tag name: {} to parameters array", searchParams.getTag()::getName);
            argsList.add(searchParams.getTag()
                                     .getName());
        }
        if (searchParams.getSearchText() != null) {
            LOGGER.debug("Adding searching text: {} to parameters array", searchParams::getSearchText);
            argsList.add("%" + searchParams.getSearchText() + "%"); //name pattern
            argsList.add("%" + searchParams.getSearchText() + "%"); //description pattern
        }
        LOGGER.debug("Composed parameters list: {}", argsList);
        return argsList.toArray();
    }


    private void addTagNameToSearch(StringBuilder searchQuery) {
        searchQuery.insert(searchQuery.indexOf(GROUP_BY), "WHERE tag.name = ? ");
    }

    private void addTextToSearch(StringBuilder searchQuery) {
        if (searchQuery.indexOf("WHERE") != -1) {
            searchQuery.insert(searchQuery.indexOf(GROUP_BY), "AND (gift_certificate.NAME LIKE ? OR DESCRIPTION LIKE ?) ");
        } else {
            searchQuery.insert(searchQuery.indexOf(GROUP_BY), "WHERE (gift_certificate.NAME LIKE ? OR DESCRIPTION LIKE ?) ");
        }
    }

    private void addSortColumnToSearch(StringBuilder searchQuery, String sortColumn, String sortOrder) {
        if (searchQuery.indexOf(ORDER_BY) != -1) {
            searchQuery.append(", ")
                       .append(sortColumn)
                       .append(" ")
                       .append(sortOrder);
        } else {
            searchQuery.append(" " + ORDER_BY + " ")
                       .append(sortColumn)
                       .append(" ")
                       .append(sortOrder);
        }
    }
}
