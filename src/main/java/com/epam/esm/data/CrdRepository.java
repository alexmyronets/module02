package com.epam.esm.data;

import java.util.List;

public interface CrdRepository<T> {

    T create(T t);

    T read(long id);

    void delete(long id);

    List<T> list();
}
