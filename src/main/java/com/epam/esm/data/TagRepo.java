package com.epam.esm.data;

import com.epam.esm.domain.Tag;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface TagRepo extends CrdRepository<Tag> {

    Logger LOGGER = LogManager.getLogger(TagRepo.class);

    default Tag mapRowToTag(ResultSet rs, int rowNum) throws SQLException {
        LOGGER.debug("Mapping result Set to Tag");
        Tag tag = new Tag();

        tag.setId(rs.getLong("id"));
        tag.setName(rs.getString("name"));
        return tag;
    }
}
