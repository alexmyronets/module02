package com.epam.esm.data;

import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.SearchParametersDTO;
import java.util.List;

public interface SearchRepo {

    List<GiftCertificate> readGiftCertificatesBySearchParams(SearchParametersDTO searchParams);
}
