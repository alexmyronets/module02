package com.epam.esm.data;

import com.epam.esm.domain.GiftCertificate;

public interface GiftCertRepo extends CrudRepository<GiftCertificate> {

}
