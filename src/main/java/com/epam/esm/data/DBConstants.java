package com.epam.esm.data;

public abstract class DBConstants {

    //SQL SELECT statements
    public static final String SQL_QUERY_GIFT_CERTIFICATE = """
        SELECT *
        FROM gift_certificate
        WHERE id = ?""";
    public static final String SQL_QUERY_ALL_GIFT_CERTIFICATES = """
        SELECT *
        FROM gift_certificate""";
    public static final String SQL_QUERY_TAGS_BY_GIFT_CERTIFICATE = """
        SELECT tag.id,
               tag.name
        FROM tag
        JOIN gift_certificate_tag ON tag.id = tag_id
        JOIN gift_certificate ON gift_certificate_id = gift_certificate.id
        WHERE gift_certificate.id = ?""";
    public static final String SQL_QUERY_TAG = """
        SELECT *
        FROM tag
        WHERE id = ?""";
    public static final String SQL_QUERY_TAG_BY_NAME = """
        SELECT *
        FROM tag
        WHERE name = ?""";
    public static final String SQL_QUERY_ALL_TAGS = """
        SELECT *
        FROM tag""";
    public static final String SQL_QUERY_SEARCH_TEMPLATE = """
        SELECT gift_certificate.id
        FROM gift_certificate
        JOIN gift_certificate_tag ON gift_certificate.id = gift_certificate_id
        JOIN tag ON tag_id = tag.id
        GROUP BY gift_certificate.id""";
    public static final String SQL_QUERY_COUNT_CERTIFICATES_BY_TAG = """
        SELECT COUNT(*)
        FROM
          (SELECT tag.id
           FROM tag
           JOIN gift_certificate_tag ON tag.id = tag_id
           WHERE tag.id = ?) AS amount""";

    //SQL INSERT statements
    public static final String SQL_INSERT_GIFT_CERTIFICATE = """
        INSERT INTO gift_certificate (name, description, price, duration, create_date, last_update_date)
        VALUES (?, ?, ?, ?, ?, ?)""";
    public static final String SQL_INSERT_GIFT_CERTIFICATE_TAG_REFERENCE = """
        INSERT INTO gift_certificate_tag (gift_certificate_id, tag_id)
        VALUES (?, ?)""";
    public static final String SQL_INSERT_TAG = """
        INSERT INTO tag (name)
        VALUES (?)""";

    //SQL UPDATE statements
    public static final String SQL_UPDATE_GIFT_CERTIFICATE = """
        UPDATE gift_certificate
        SET name = ?,
            description = ?,
            price = ?,
            duration = ?,
            last_update_date = ?
        WHERE id = ?""";

    //SQL DELETE statements
    public static final String SQL_DELETE_GIFT_CERTIFICATE = """
        DELETE
        FROM gift_certificate
        WHERE id = ?""";
    public static final String SQL_DELETE_GIFT_CERTIFICATE_TAG_REFERENCE = """
        DELETE
        FROM gift_certificate_tag
        WHERE gift_certificate_id = ?
          AND tag_id = ?""";
    public static final String SQL_DELETE_TAG = """
        DELETE
        FROM tag
        WHERE id = ?""";

    //SQL keywords
    public static final String GROUP_BY = "GROUP BY";
    public static final String ORDER_BY = "ORDER BY";

    private DBConstants() {
    }
}
