package com.epam.esm.data;

public interface CrudRepository<T> extends CrdRepository<T> {

    T update(T t, long id);
}
