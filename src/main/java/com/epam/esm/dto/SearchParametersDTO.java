package com.epam.esm.dto;

import com.epam.esm.domain.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;
import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchParametersDTO implements Serializable {

    @Serial
    private static final long serialVersionUID = 6650179392535547921L;

    @Valid
    private Tag tag;

    @Size(min = 1, message = "Search text cannot be blank")
    private String searchText;
    private SearchOrder nameSearchOrder;
    private SearchOrder dateSearchOrder;

    public enum SearchOrder {
        ASC, DESC
    }
}
