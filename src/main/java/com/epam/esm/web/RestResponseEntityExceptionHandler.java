package com.epam.esm.web;

import com.epam.esm.exception.NoSuchGiftCertificateException;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = NoSuchGiftCertificateException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchGiftCertificateException(NoSuchGiftCertificateException e) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40401"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = NoSuchTagException.class)
    public ResponseEntity<ErrorResponse> handleNoSuchTagException(NoSuchTagException e) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40402"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = TagAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handleTagAlreadyExistsException(TagAlreadyExistsException e) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40902"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = TagIsInUseException.class)
    public ResponseEntity<ErrorResponse> handleTagIsInUseException(TagIsInUseException e) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse(e.getMessage(), "40901"), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception e) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse("Internal server error", "50001"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    @Nullable
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatusCode status,
                                                                  WebRequest request) {
        logger.error(e);
        Map<String, String> errors = new HashMap<>();
        ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse();
        ResponseEntity<Object> responseEntity = new ResponseEntity<>(validationErrorResponse, HttpStatus.BAD_REQUEST);

        validationErrorResponse.setErrorMessage("Validation error");
        validationErrorResponse.setErrorCode("40002");

        e.getBindingResult()
         .getAllErrors()
         .forEach(error -> {
             String fieldName = ((FieldError) error).getField();
             String errorMessage = error.getDefaultMessage();
             errors.put(fieldName, errorMessage);
         });
        validationErrorResponse.setValidationErrors(errors);
        return responseEntity;
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException e, HttpHeaders headers, HttpStatusCode status,
                                                                   WebRequest request) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse("Requested resource not found: " + e.getMessage(), "40000"), HttpStatus.NOT_FOUND);
    }

    @Override
    @Nullable
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e, HttpHeaders headers, HttpStatusCode status,
                                                                  WebRequest request) {
        logger.error(e);
        return new ResponseEntity<>(new ErrorResponse("Failed to read request", "40001"), HttpStatus.BAD_REQUEST);
    }

    @Data
    @RequiredArgsConstructor
    @AllArgsConstructor
    private static class ErrorResponse {

        private String errorMessage;
        private String errorCode;

    }

    @Data
    private static class ValidationErrorResponse extends ErrorResponse {

        Map<String, String> validationErrors;
    }
}
