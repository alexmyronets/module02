package com.epam.esm.web;

import com.epam.esm.data.TagRepo;
import com.epam.esm.domain.Tag;
import jakarta.validation.Valid;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for CRD operation of {@link Tag}.
 *
 * @author Oleksandr Myronets
 */
@RestController
@RequestMapping(path = "/api/tag", produces = MediaType.APPLICATION_JSON_VALUE)
public class TagController {

    private static final Logger LOGGER = LogManager.getLogger(TagController.class);

    private final TagRepo tagRepo;

    public TagController(TagRepo tagRepo) {
        this.tagRepo = tagRepo;
    }

    /**
     * Performs create operation for Tag.
     *
     * @param tag {@link Tag} in JSON format.
     * @return created {@link Tag} in JSON format.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Tag postTag(@Valid @RequestBody Tag tag) {
        LOGGER.info("POST request for tag: {}", tag);
        return tagRepo.create(tag);
    }

    /**
     * Performs list operation for Tag.
     *
     * @return List of {@link Tag} in JSON format.
     */
    @GetMapping
    public List<Tag> getTagsList() {
        LOGGER.info("GET request for the list of all tags");
        return tagRepo.list();
    }

    /**
     * Performs read operation for Tag.
     *
     * @param id id of Tag to read.
     * @return {@link Tag} in JSON format.
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Tag getTag(@PathVariable Long id) {
        LOGGER.info("GET request for tag with id = {}", id);
        return tagRepo.read(id);
    }

    /**
     * Performs delete operation for Tag.
     *
     * @param id id of Tag to delete.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTag(@PathVariable Long id) {
        LOGGER.info("DELETE request for tag with id = {}", id);
        tagRepo.delete(id);
    }
}
