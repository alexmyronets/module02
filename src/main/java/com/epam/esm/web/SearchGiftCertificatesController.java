package com.epam.esm.web;

import com.epam.esm.data.SearchRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.dto.SearchParametersDTO;
import jakarta.validation.Valid;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class manages Gift Certificate search requests.
 *
 * @author Oleksand Myronets.
 */
@RestController
@RequestMapping(path = "/api/search", produces = MediaType.APPLICATION_JSON_VALUE)
public class SearchGiftCertificatesController {

    private static final Logger LOGGER = LogManager.getLogger(SearchGiftCertificatesController.class);

    private final SearchRepo searchRepo;

    public SearchGiftCertificatesController(SearchRepo searchRepo) {
        this.searchRepo = searchRepo;
    }

    /**
     * Performs search by specified parameters.
     *
     * @param searchParams parameters of search in JSON format.
     * @return List of {@link GiftCertificate} in JSON format.
     */
    @GetMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<GiftCertificate> searchGiftCertificates(@Valid @RequestBody SearchParametersDTO searchParams) {
        LOGGER.info("GET request for search Gift Certificates by search parameters: {}", searchParams);
        return searchRepo.readGiftCertificatesBySearchParams(searchParams);
    }
}
