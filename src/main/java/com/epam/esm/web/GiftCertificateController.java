package com.epam.esm.web;

import com.epam.esm.data.GiftCertRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.validation.PatchInfo;
import jakarta.validation.Valid;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This class consists of methods for managing requests for CRUD operation of {@link GiftCertificate}.
 *
 * @author Oleksandr Myronets.
 */
@RestController
@RequestMapping(path = "/api/certificate", produces = MediaType.APPLICATION_JSON_VALUE)
public class GiftCertificateController {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateController.class);

    private final GiftCertRepo giftCertRepo;

    public GiftCertificateController(GiftCertRepo giftCertRepo) {
        this.giftCertRepo = giftCertRepo;
    }

    /**
     * Performs create operation for Gift Certificate.
     *
     * @param giftCert - {@link GiftCertificate} to create in JSON format.
     * @return created {@link GiftCertificate} in JSON format.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public GiftCertificate postGiftCertificate(@Valid @RequestBody GiftCertificate giftCert) {
        LOGGER.info("POST request for Gift Certificate: {}", giftCert);
        return giftCertRepo.create(giftCert);
    }

    /**
     * Performs list operation for Gift Certificate.
     *
     * @return List of {@link GiftCertificate} in JSON format.
     */
    @GetMapping
    public List<GiftCertificate> getGitCertificatesList() {
        LOGGER.info("GET request for the list of all certificates");
        return giftCertRepo.list();
    }

    /**
     * Performs read operation for Gift Certificate.
     *
     * @param id id of Gift Certificate to read.
     * @return {@link GiftCertificate} in JSON format.
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public GiftCertificate getGiftCertificate(@PathVariable Long id) {
        LOGGER.info("GET request for certificate with id = {}", id);
        return giftCertRepo.read(id);
    }

    /**
     * Performs update operation for Gift Certificate.
     *
     * @param id       of Gift Certificate to update.
     * @param giftCert {@link GiftCertificate} with patch details.
     * @return patched {@link GiftCertificate} in JSON format.
     */
    @PatchMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GiftCertificate patchGiftCertificate(@PathVariable Long id, @Validated(PatchInfo.class) @RequestBody GiftCertificate giftCert) {
        LOGGER.info("PATCH request for certificate with id = {}, patch details: {}", id, giftCert);
        return giftCertRepo.update(giftCert, id);
    }

    /**
     * Performs delete operation for Gift Certificate.
     *
     * @param id id of Gift Certificate to delete.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGiftCertificate(@PathVariable Long id) {
        LOGGER.info("DELETE request for certificate with id = {}", id);
        giftCertRepo.delete(id);
    }
}
