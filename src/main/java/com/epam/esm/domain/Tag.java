package com.epam.esm.domain;

import com.epam.esm.validation.PatchInfo;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import jakarta.validation.groups.Default;
import java.io.Serial;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tag implements Serializable {

    @Serial
    private static final long serialVersionUID = -7537350846367346043L;

    private Long id;

    @NotNull(message = "Tag name is mandatory", groups = {Default.class, PatchInfo.class})
    @Size(min = 3, max = 10, message = "Tag name cannot be shorter than 3 characters and longer than 10 characters", groups = {Default.class,
        PatchInfo.class})
    private String name;
}
