CREATE TABLE IF NOT EXISTS gift_certificate
(
id BIGSERIAL PRIMARY KEY,
name VARCHAR NOT NULL,
description VARCHAR NOT NULL,
price DECIMAL(10,2) NOT NULL,
duration SMALLINT NOT NULL,
create_date TIMESTAMP NOT NULL,
last_update_date TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS tag
(
id BIGSERIAL PRIMARY KEY,
name VARCHAR UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS gift_certificate_tag
(
gift_certificate_id BIGINT REFERENCES gift_certificate (id) ON DELETE CASCADE,
tag_id BIGINT REFERENCES tag (id) ON DELETE RESTRICT,
CONSTRAINT gift_certificate_tag_pkey PRIMARY KEY (gift_certificate_id, tag_id)
)
