package com.epam.esm.web;

import static org.hamcrest.Matchers.matchesPattern;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.config.RootConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootConfig.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@WebAppConfiguration
class GiftCertificateControllerRepositoryIntegrationTest {

    private static final String ISO_8601_DATE_REGEX = """
        ^(?:[1-9]\\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\\d|2[0-3]):[0-5]\\d:[0-5]\\d.([0-9]{3})$""";

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                                      .build();
    }

    @Test
    void postValidGiftCert_thenCreated() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert6Name",
                "description": "cert6Description",
                "price": 50,
                "duration": 90,
                "tags": [
                    {
                        "name": "tag12"
                    }
                ]
            }""";

        this.mockMvc.perform(post("/api/certificate").content(requestBodyJson)
                                                     .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("$.id").value("6"))
                    .andExpect(jsonPath("$.name").value("cert6Name"))
                    .andExpect(jsonPath("$.description").value("cert6Description"))
                    .andExpect(jsonPath("$.price").value("50.0"))
                    .andExpect(jsonPath("$.duration").value("90"))
                    .andExpect(jsonPath("$.createDate", matchesPattern(ISO_8601_DATE_REGEX)))
                    .andExpect(jsonPath("$.lastUpdateDate", matchesPattern(ISO_8601_DATE_REGEX)))
                    .andExpect(jsonPath("$.tags[0].id").value("12"))
                    .andExpect(jsonPath("$.tags[0].name").value("tag12"));
    }

    @Test
    void getGiftCertificatesList_thenOk() throws Exception {
        String responseBodyJson = """
            [
                 {
                     "id": 1,
                     "name": "cert1",
                     "description": "description1",
                     "price": 100.00,
                     "duration": 90,
                     "createDate": "2022-12-30T15:07:30.965",
                     "lastUpdateDate": "2022-12-30T15:07:30.965",
                     "tags": [
                         {
                             "id": 1,
                             "name": "tag1"
                         },
                         {
                             "id": 10,
                             "name": "tag10"
                         }
                     ]
                 },
                 {
                     "id": 2,
                     "name": "cert2",
                     "description": "description2",
                     "price": 150.00,
                     "duration": 30,
                     "createDate": "2022-12-30T15:25:05.523",
                     "lastUpdateDate": "2022-12-30T15:25:05.523",
                     "tags": [
                         {
                             "id": 2,
                             "name": "tag2"
                         },
                         {
                             "id": 9,
                             "name": "tag9"
                         }
                     ]
                 },
                 {
                     "id": 3,
                     "name": "cert3",
                     "description": "description3",
                     "price": 200.00,
                     "duration": 60,
                     "createDate": "2023-01-02T18:15:51.505",
                     "lastUpdateDate": "2023-01-02T18:15:51.505",
                     "tags": [
                         {
                             "id": 3,
                             "name": "tag3"
                         },
                         {
                             "id": 8,
                             "name": "tag8"
                         }
                     ]
                 },
                 {
                     "id": 4,
                     "name": "cert4",
                     "description": "description4",
                     "price": 220.50,
                     "duration": 120,
                     "createDate": "2023-01-04T00:20:46.926",
                     "lastUpdateDate": "2023-01-04T00:20:46.926",
                     "tags": [
                         {
                             "id": 4,
                             "name": "tag4"
                         },
                         {
                             "id": 7,
                             "name": "tag7"
                         }
                     ]
                 },
                 {
                     "id": 5,
                     "name": "cert5",
                     "description": "description5",
                     "price": 123.99,
                     "duration": 180,
                     "createDate": "2023-01-04T01:22:25.023",
                     "lastUpdateDate": "2023-01-04T01:22:25.023",
                     "tags": [
                         {
                             "id": 5,
                             "name": "tag5"
                         },
                         {
                             "id": 6,
                             "name": "tag6"
                         }
                     ]
                 }
            ]""";

        mockMvc.perform(MockMvcRequestBuilders.get("/api/certificate"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingGiftCert_thenOk() throws Exception {
        String responseBodyJson = """
            {
                "id": 1,
                "name": "cert1",
                "description": "description1",
                "price": 100.00,
                "duration": 90,
                "createDate": "2022-12-30T15:07:30.965",
                "lastUpdateDate": "2022-12-30T15:07:30.965",
                "tags": [
                    {
                        "id": 1,
                        "name": "tag1"
                    },
                    {
                        "id": 10,
                        "name": "tag10"
                    }
                ]
            }""";

        mockMvc.perform(MockMvcRequestBuilders.get("/api/certificate/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        mockMvc.perform(get("/api/certificate/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchValidGiftCert_thenOk() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag12"
                    }
                ]
            }""";

        mockMvc.perform(patch("/api/certificate/1").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.id").value("1"))
               .andExpect(jsonPath("$.name").value("cert1NamePatched"))
               .andExpect(jsonPath("$.description").value("cert1DescriptionPatched"))
               .andExpect(jsonPath("$.price").value("500.0"))
               .andExpect(jsonPath("$.duration").value("120"))
               .andExpect(jsonPath("$.createDate").value("2022-12-30T15:07:30.965"))
               .andExpect(jsonPath("$.lastUpdateDate", matchesPattern(ISO_8601_DATE_REGEX)))
               .andExpect(jsonPath("$.tags[0].id").value("12"))
               .andExpect(jsonPath("$.tags[0].name").value("tag12"));
    }

    @Test
    void patchNonExistingGiftCert_thenNotFound() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag10Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        mockMvc.perform(patch("/api/certificate/42").content(requestBodyJson)
                                                    .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteExistingGiftCert_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/certificate/1"))
               .andExpect(status().isNoContent());
    }

    @Test
    void deleteNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        mockMvc.perform(delete("/api/certificate/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }
}
