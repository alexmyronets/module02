package com.epam.esm.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.epam.esm.data.SearchRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.Tag;
import com.epam.esm.dto.SearchParametersDTO;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

class SearchGiftCertificatesControllerTest {

    static SearchRepo mockSearchRepo = mock(SearchRepo.class);


    @BeforeAll
    static void setupMockSearchRepo() {
        var giftCertListToReturn = Arrays.asList(new GiftCertificate(1L,
                                                                     "cert1holiday",
                                                                     "cert1Description",
                                                                     new BigDecimal("50"),
                                                                     (short) 90,
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     new ArrayList<>(Arrays.asList(new Tag(1L, "tag1"),
                                                                                                   new Tag(2L, "tag2Name"),
                                                                                                   new Tag(3L, "tag3Name")))),
                                                 new GiftCertificate(2L,
                                                                     "cert2holiday",
                                                                     "cert2Description",
                                                                     new BigDecimal("55"),
                                                                     (short) 100,
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     LocalDateTime.of(2023, 2, 7, 13, 30, 30, 100),
                                                                     new ArrayList<>(Arrays.asList(new Tag(2L, "tag2Name"),
                                                                                                   new Tag(1L, "tag1"),
                                                                                                   new Tag(4L, "tag4Name")))),
                                                 new GiftCertificate(3L,
                                                                     "cert3holiday",
                                                                     "cert3Description",
                                                                     new BigDecimal("60"),
                                                                     (short) 110,
                                                                     LocalDateTime.of(2023, 2, 7, 13, 30, 30, 100),
                                                                     LocalDateTime.of(2023, 3, 9, 14, 45, 20, 123),
                                                                     new ArrayList<>(Arrays.asList(new Tag(1L, "tag1"),
                                                                                                   new Tag(4L, "tag4Name"),
                                                                                                   new Tag(5L, "tag5Name")))));

        when(mockSearchRepo.readGiftCertificatesBySearchParams(any(SearchParametersDTO.class))).thenReturn(giftCertListToReturn);
    }

    private static Stream<String> provideRequestBodyJson() {
        return Stream.of("""
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "searchText": "e",
                                 "nameSearchOrder": "ASC",
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "searchText": "e",
                                 "nameSearchOrder": "ASC",
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "nameSearchOrder": "ASC",
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "searchText": "e",
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "searchText": "e",
                                 "nameSearchOrder": "ASC"
                             }""", """
                             {}
                             """, """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "searchText": "e"
                             }
                             """, """
                             {
                                 "nameSearchOrder": "ASC",
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "dateSearchOrder": "DESC"
                             }""", """
                             {
                                 "searchText": "e",
                                 "nameSearchOrder": "ASC"
                             }""", """
                             {
                                 "tag": {
                                     "id": 20,
                                     "name": "tag2"
                                 },
                                 "nameSearchOrder": "ASC"
                             }""", """
                             {
                                 "searchText": "e",
                                 "dateSearchOrder": "DESC"
                             }""");

    }

    @ParameterizedTest
    @MethodSource("provideRequestBodyJson")
    void getValidSearch_thenOk(String requestBodyJson) throws Exception {
        String responseBodyJson = """
            [
                {
                    "id": 1,
                    "name": "cert1holiday",
                    "description": "cert1Description",
                    "price": 50,
                    "duration": 90,
                    "createDate": "2023-01-06T12:00:00.000",
                    "lastUpdateDate": "2023-01-06T12:00:00.000",
                    "tags": [
                        {
                            "id": 1,
                            "name": "tag1"
                        },
                        {
                            "id": 2,
                            "name": "tag2Name"
                        },
                        {
                            "id": 3,
                            "name": "tag3Name"
                        }
                    ]
                },
                {
                    "id": 2,
                    "name": "cert2holiday",
                    "description": "cert2Description",
                    "price": 55,
                    "duration": 100,
                    "createDate": "2023-01-06T12:00:00.000",
                    "lastUpdateDate": "2023-02-07T13:30:30.000",
                    "tags": [
                        {
                            "id": 2,
                            "name": "tag2Name"
                        },
                        {
                            "id": 1,
                            "name": "tag1"
                        },
                        {
                            "id": 4,
                            "name": "tag4Name"
                        }
                    ]
                },
                {
                    "id": 3,
                    "name": "cert3holiday",
                    "description": "cert3Description",
                    "price": 60,
                    "duration": 110,
                    "createDate": "2023-02-07T13:30:30.000",
                    "lastUpdateDate": "2023-03-09T14:45:20.000",
                    "tags": [
                        {
                            "id": 1,
                            "name": "tag1"
                        },
                        {
                            "id": 4,
                            "name": "tag4Name"
                        },
                        {
                            "id": 5,
                            "name": "tag5Name"
                        }
                    ]
                }
            ]""";
        SearchGiftCertificatesController controller = new SearchGiftCertificatesController(mockSearchRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/api/search").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "tag": {
                    "id": "asd20",
                    "name": "tag2"
                },
                "searchText": 42,
                "nameSearchOrder": "ASdddC",
                "dateSearchOrder": "DEddddSC"
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Failed to read request",
                "errorCode": "40001"
            }""";

        SearchGiftCertificatesController controller = new SearchGiftCertificatesController(mockSearchRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(get("/api/search").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }

    @Test
    void getInvalidSearch_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "tag": {
                    "id": 1,
                    "name": "ta"
                },
                "searchText": "",
                "nameSearchOrder": "ASC",
                "dateSearchOrder": "DESC"
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Validation error",
                "errorCode": "40002",
                "validationErrors": {
                    "searchText": "Search text cannot be blank",
                    "tag.name": "Tag name cannot be shorter than 3 characters and longer than 10 characters"
                }
            }""";

        SearchGiftCertificatesController controller = new SearchGiftCertificatesController(mockSearchRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(get("/api/search").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }
}