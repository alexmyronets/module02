package com.epam.esm.web;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.epam.esm.data.TagRepo;
import com.epam.esm.domain.Tag;
import com.epam.esm.exception.NoSuchTagException;
import com.epam.esm.exception.TagAlreadyExistsException;
import com.epam.esm.exception.TagIsInUseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

class TagControllerTest {

    static TagRepo mockTagRepo = mock(TagRepo.class);

    @BeforeAll
    static void setupMockTagRepo() {
        Tag tagToCreate = new Tag(null, "Tag1");
        Tag tagToReturn = new Tag(1L, "Tag1");
        Tag tagExists = new Tag(null, "Tag5");
        List<Tag> tags = new ArrayList<>(Arrays.asList(new Tag(1L, "Tag1"),
                                                       new Tag(2L, "Tag2"),
                                                       new Tag(3L, "Tag3"),
                                                       new Tag(4L, "Tag4"),
                                                       new Tag(5L, "Tag5")));

        when(mockTagRepo.create(tagToCreate)).thenReturn(tagToReturn);
        when(mockTagRepo.create(tagExists)).thenThrow(new TagAlreadyExistsException("Tag (name = Tag5) is already exists"));
        when(mockTagRepo.list()).thenReturn(tags);
        when(mockTagRepo.read(1)).thenReturn(tagToReturn);
        when(mockTagRepo.read(42)).thenThrow(new NoSuchTagException("Requested resource not found (id = 42)"));
        doThrow(new TagIsInUseException("Tag (id = 5) is in use by 10 gift certificates")).when(mockTagRepo)
                                                                                          .delete(5);
        doThrow(new NoSuchTagException("Requested resource not found (id = 42)")).when(mockTagRepo)
                                                                                 .delete(42);
    }

    @Test
    void postValidTag_thenCreated() throws Exception {
        String requestBodyJson = """
            {
                "name": "Tag1"
            }""";
        String responseBodyJson = """
            {
                "id": 1,
                "name": "Tag1"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "name": Tag1
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Failed to read request",
                "errorCode": "40001"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidTag_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "name": "T"
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Validation error",
                "errorCode": "40002",
                "validationErrors": {
                    "name": "Tag name cannot be shorter than 3 characters and longer than 10 characters"
                }
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postExistingTag_thenConflict() throws Exception {
        String requestBodyJson = """
            {
                "name": "Tag5"
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Tag (name = Tag5) is already exists",
                "errorCode": "40902"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getTagsList_thenOk() throws Exception {
        String responseBodyJson = """
            [
                {
                    "id": 1,
                    "name": "Tag1"
                },
                {
                    "id": 2,
                    "name": "Tag2"
                },
                {
                    "id": 3,
                    "name": "Tag3"
                },
                {
                    "id": 4,
                    "name": "Tag4"
                },
                {
                    "id": 5,
                    "name": "Tag5"
                }
            ]""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/api/tag"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingTag_thenOk() throws Exception {
        String responseBodyJson = """
            {
                "id": 1,
                "name": "Tag1"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(get("/api/tag/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40402"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(get("/api/tag/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotUsedTag_thenNoContent() throws Exception {
        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(delete("/api/tag/1"))
               .andExpect(status().isNoContent());
    }

    @Test
    void deleteUsedTag_thenConflict() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Tag (id = 5) is in use by 10 gift certificates",
                "errorCode": "40901"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(delete("/api/tag/5"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40402"
            }""";

        TagController controller = new TagController(mockTagRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(delete("/api/tag/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }
}