package com.epam.esm.web;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.epam.esm.data.GiftCertRepo;
import com.epam.esm.domain.GiftCertificate;
import com.epam.esm.domain.Tag;
import com.epam.esm.exception.NoSuchGiftCertificateException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

class GiftCertificateControllerTest {

    static GiftCertRepo mockGiftCertRepo = mock(GiftCertRepo.class);

    @BeforeAll
    static void setupMockGiftCertRepo() {
        var giftCertToSave = new GiftCertificate(null,
                                                 "cert1Name",
                                                 "cert1Description",
                                                 new BigDecimal("50"),
                                                 (short) 90,
                                                 null,
                                                 null,
                                                 new ArrayList<>(List.of(new Tag(null, "tag1Name"))));

        var giftCertToReturn = new GiftCertificate(1L,
                                                   "cert1Name",
                                                   "cert1Description",
                                                   new BigDecimal("50"),
                                                   (short) 90,
                                                   LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                   LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                   new ArrayList<>(List.of(new Tag(1L, "tag1Name"))));

        var giftCertListToReturn = Arrays.asList(new GiftCertificate(1L,
                                                                     "cert1Name",
                                                                     "cert1Description",
                                                                     new BigDecimal("50"),
                                                                     (short) 90,
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     new ArrayList<>(Arrays.asList(new Tag(1L, "tag1Name"),
                                                                                                   new Tag(2L, "tag2Name"),
                                                                                                   new Tag(3L, "tag3Name")))),
                                                 new GiftCertificate(2L,
                                                                     "cert2Name",
                                                                     "cert2Description",
                                                                     new BigDecimal("55"),
                                                                     (short) 100,
                                                                     LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                                     LocalDateTime.of(2023, 2, 7, 13, 30, 30, 100),
                                                                     new ArrayList<>(Arrays.asList(new Tag(2L, "tag2Name"),
                                                                                                   new Tag(3L, "tag3Name"),
                                                                                                   new Tag(4L, "tag4Name")))),
                                                 new GiftCertificate(3L,
                                                                     "cert3Name",
                                                                     "cert3Description",
                                                                     new BigDecimal("60"),
                                                                     (short) 110,
                                                                     LocalDateTime.of(2023, 2, 7, 13, 30, 30, 100),
                                                                     LocalDateTime.of(2023, 3, 9, 14, 45, 20, 123),
                                                                     new ArrayList<>(Arrays.asList(new Tag(3L, "tag3Name"),
                                                                                                   new Tag(4L, "tag4Name"),
                                                                                                   new Tag(5L, "tag5Name")))),
                                                 new GiftCertificate(4L,
                                                                     "cert4Name",
                                                                     "cert4Description",
                                                                     new BigDecimal("65"),
                                                                     (short) 120,
                                                                     LocalDateTime.of(2023, 3, 9, 14, 45, 20, 123),
                                                                     LocalDateTime.of(2023, 4, 12, 18, 58, 58, 777),
                                                                     new ArrayList<>(Arrays.asList(new Tag(4L, "tag4Name"),
                                                                                                   new Tag(5L, "tag5Name"),
                                                                                                   new Tag(6L, "tag6Name")))));
        var giftCertPatch = new GiftCertificate(null,
                                                "cert1NamePatched",
                                                "cert1DescriptionPatched",
                                                new BigDecimal("500"),
                                                (short) 120,
                                                null,
                                                null,
                                                new ArrayList<>(List.of(new Tag(null, "tag10Name"))));

        var giftCertPatched = new GiftCertificate(1L,
                                                  "cert1NamePatched",
                                                  "cert1DescriptionPatched",
                                                  new BigDecimal("500"),
                                                  (short) 120,
                                                  LocalDateTime.of(2023, 1, 6, 12, 0, 0, 0),
                                                  LocalDateTime.of(2023, 1, 7, 18, 0, 0, 0),
                                                  new ArrayList<>(List.of(new Tag(10L, "tag10Name"))));

        when(mockGiftCertRepo.create(giftCertToSave)).thenReturn(giftCertToReturn);
        when(mockGiftCertRepo.list()).thenReturn(giftCertListToReturn);
        when(mockGiftCertRepo.read(1)).thenReturn(giftCertToReturn);
        when(mockGiftCertRepo.read(42)).thenThrow(new NoSuchGiftCertificateException("Requested resource not found (id = 42)"));
        when(mockGiftCertRepo.update(giftCertPatch, 1)).thenReturn(giftCertPatched);
        when(mockGiftCertRepo.update(giftCertPatch, 42)).thenThrow(new NoSuchGiftCertificateException("Requested resource not found (id = 42)"));
        doThrow(new NoSuchGiftCertificateException("Requested resource not found (id = 42)")).when(mockGiftCertRepo)
                                                                                             .delete(42);
        when(mockGiftCertRepo.update(giftCertPatch, 500)).thenThrow(new RuntimeException());

    }

    @Test
    void postValidGiftCert_thenCreated() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1Name",
                "description": "cert1Description",
                "price": 50,
                "duration": 90,
                "tags": [
                    {
                        "name": "tag1Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "id": 1,
                "name": "cert1Name",
                "description": "cert1Description",
                "price": 50,
                "duration": 90,
                "createDate": "2023-01-06T12:00:00.000",
                "lastUpdateDate": "2023-01-06T12:00:00.000",
                "tags": [
                    {
                        "id": 1,
                        "name": "tag1Name"
                    }
                ]
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(post("/api/certificate").content(requestBodyJson)
                                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1Name",
                "description": "cert1Description",
                "price": "INVALID_PRICE_FORMAT",
                "duration": 90,
                "tags": [
                    {
                        "name": "tag1Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Failed to read request",
                "errorCode": "40001"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(post("/api/certificate").content(requestBodyJson)
                                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postInvalidGiftCert_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "name": "ce",
                "description": "Desc",
                "price": 0,
                "duration": -90,
                "tags": [
                    {
                        "name": "tagNameMuchLongerThanShouldBeByValidation"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Validation error",
                "errorCode": "40002",
                "validationErrors": {
                    "duration": "Minimum duration is 1",
                    "price": "Minimum price is 1",
                    "name": "Name cannot be shorter than 3 characters and longer than 16 characters",
                    "description": "Description cannot be shorter than 5 characters and longer than 50 characters",
                    "tags[0].name": "Tag name cannot be shorter than 3 characters and longer than 10 characters"
                }
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(post("/api/certificate").content(requestBodyJson)
                                                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getGiftCertificatesList_thenOk() throws Exception {
        String responseBodyJson = """
            [
                {
                    "id": 1,
                    "name": "cert1Name",
                    "description": "cert1Description",
                    "price": 50,
                    "duration": 90,
                    "createDate": "2023-01-06T12:00:00.000",
                    "lastUpdateDate": "2023-01-06T12:00:00.000",
                    "tags": [
                        {
                            "id": 1,
                            "name": "tag1Name"
                        },
                        {
                            "id": 2,
                            "name": "tag2Name"
                        },
                        {
                            "id": 3,
                            "name": "tag3Name"
                        }
                    ]
                },
                {
                    "id": 2,
                    "name": "cert2Name",
                    "description": "cert2Description",
                    "price": 55,
                    "duration": 100,
                    "createDate": "2023-01-06T12:00:00.000",
                    "lastUpdateDate": "2023-02-07T13:30:30.000",
                    "tags": [
                        {
                            "id": 2,
                            "name": "tag2Name"
                        },
                        {
                            "id": 3,
                            "name": "tag3Name"
                        },
                        {
                            "id": 4,
                            "name": "tag4Name"
                        }
                    ]
                },
                {
                    "id": 3,
                    "name": "cert3Name",
                    "description": "cert3Description",
                    "price": 60,
                    "duration": 110,
                    "createDate": "2023-02-07T13:30:30.000",
                    "lastUpdateDate": "2023-03-09T14:45:20.000",
                    "tags": [
                        {
                            "id": 3,
                            "name": "tag3Name"
                        },
                        {
                            "id": 4,
                            "name": "tag4Name"
                        },
                        {
                            "id": 5,
                            "name": "tag5Name"
                        }
                    ]
                },
                {
                    "id": 4,
                    "name": "cert4Name",
                    "description": "cert4Description",
                    "price": 65,
                    "duration": 120,
                    "createDate": "2023-03-09T14:45:20.000",
                    "lastUpdateDate": "2023-04-12T18:58:58.000",
                    "tags": [
                        {
                            "id": 4,
                            "name": "tag4Name"
                        },
                        {
                            "id": 5,
                            "name": "tag5Name"
                        },
                        {
                            "id": 6,
                            "name": "tag6Name"
                        }
                    ]
                }
            ]""";
        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/certificate"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingGiftCert_thenOk() throws Exception {
        String responseBodyJson = """
            {
                "id": 1,
                "name": "cert1Name",
                "description": "cert1Description",
                "price": 50,
                "duration": 90,
                "createDate": "2023-01-06T12:00:00.000",
                "lastUpdateDate": "2023-01-06T12:00:00.000",
                "tags": [
                    {
                        "id": 1,
                        "name": "tag1Name"
                    }
                ]
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/certificate/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(get("/api/certificate/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchValidGiftCert_thenOk() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag10Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "id": 1,
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "createDate": "2023-01-06T12:00:00.000",
                "lastUpdateDate": "2023-01-07T18:00:00.000",
                "tags": [
                    {
                        "id": 10,
                        "name": "tag10Name"
                    }
                ]
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(patch("/api/certificate/1").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchNonExistingGiftCert_thenNotFound() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag10Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(patch("/api/certificate/42").content(requestBodyJson)
                                                    .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchInvalidJsonFormat_thenBadRequest() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": cert1DescriptionPatched_BAD_FORMAT,
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag10Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Failed to read request",
                "errorCode": "40001"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(patch("/api/certificate/1").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void patchInvalidGiftCert_thenBadRequestWithErrors() throws Exception {
        String requestBodyJson = """
            {
                "name": "ce",
                "description": "cert",
                "price": -500,
                "duration": 0,
                "tags": [
                    {
                        "name": "t"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Validation error",
                "errorCode": "40002",
                "validationErrors": {
                    "duration": "Minimum duration is 1",
                    "price": "Minimum price is 1",
                    "name": "Name cannot be shorter than 3 characters and longer than 16 characters",
                    "description": "Description cannot be shorter than 5 characters and longer than 50 characters",
                    "tags[0].name": "Tag name cannot be shorter than 3 characters and longer than 10 characters"
                }
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);

        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(patch("/api/certificate/1").content(requestBodyJson)
                                                   .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteExistingGiftCert_thenNoContent() throws Exception {
        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);
        MockMvc mockMvc = standaloneSetup(controller).build();
        mockMvc.perform(delete("/api/certificate/1"))
               .andExpect(status().isNoContent());
    }

    @Test
    void deleteNonExistingGiftCert_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40401"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(delete("/api/certificate/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }

    @Test
    void unExpectedExceptionTest() throws Exception {
        String requestBodyJson = """
            {
                "name": "cert1NamePatched",
                "description": "cert1DescriptionPatched",
                "price": 500,
                "duration": 120,
                "tags": [
                    {
                        "name": "tag10Name"
                    }
                ]
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Internal server error",
                "errorCode": "50001"
            }""";

        GiftCertificateController controller = new GiftCertificateController(mockGiftCertRepo);
        MockMvc mockMvc = standaloneSetup(controller).setControllerAdvice(new RestResponseEntityExceptionHandler())
                                                     .build();
        mockMvc.perform(patch("/api/certificate/500").content(requestBodyJson)
                                                     .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isInternalServerError())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));

    }
}