package com.epam.esm.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.config.RootConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootConfig.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@WebAppConfiguration
public class TagControllerRepositoryIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                                      .build();
    }

    @Test
    void postValidTag_thenCreated() throws Exception {
        String requestBodyJson = """
            {
                "name": "tag12"
            }""";
        String responseBodyJson = """
            {
                "id": 12,
                "name": "tag12"
            }""";

        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isCreated())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void postExistingTag_thenConflict() throws Exception {
        String requestBodyJson = """
            {
                "name": "tag5"
            }""";
        String responseBodyJson = """
            {
                "errorMessage": "Tag (name = tag5) is already exists",
                "errorCode": "40902"
            }""";

        mockMvc.perform(post("/api/tag").content(requestBodyJson)
                                        .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getTagsList_thenOk() throws Exception {
        String responseBodyJson = """
            [
                {
                    "id": 1,
                    "name": "tag1"
                },
                {
                    "id": 2,
                    "name": "tag2"
                },
                {
                    "id": 3,
                    "name": "tag3"
                },
                {
                    "id": 4,
                    "name": "tag4"
                },
                {
                    "id": 5,
                    "name": "tag5"
                },
                {
                    "id": 6,
                    "name": "tag6"
                },
                {
                    "id": 7,
                    "name": "tag7"
                },
                {
                    "id": 8,
                    "name": "tag8"
                },
                {
                    "id": 9,
                    "name": "tag9"
                },
                {
                    "id": 10,
                    "name": "tag10"
                },
                {
                    "id": 11,
                    "name": "tag11"
                }
            ]""";

        mockMvc.perform(get("/api/tag"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getExistingTag_thenOk() throws Exception {
        String responseBodyJson = """
            {
                "id": 1,
                "name": "tag1"
            }""";

        mockMvc.perform(get("/api/tag/1"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void getNonExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40402"
            }""";

        mockMvc.perform(get("/api/tag/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotUsedTag_thenNoContent() throws Exception {
        mockMvc.perform(delete("/api/tag/11"))
               .andExpect(status().isNoContent());
    }

    @Test
    void deleteUsedTag_thenConflict() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Tag (id = 5) is in use by 1 gift certificates",
                "errorCode": "40901"
            }""";

        mockMvc.perform(delete("/api/tag/5"))
               .andExpect(status().isConflict())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }

    @Test
    void deleteNotExistingTag_thenNotFound() throws Exception {
        String responseBodyJson = """
            {
                "errorMessage": "Requested resource not found (id = 42)",
                "errorCode": "40402"
            }""";

        mockMvc.perform(delete("/api/tag/42"))
               .andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }
}
