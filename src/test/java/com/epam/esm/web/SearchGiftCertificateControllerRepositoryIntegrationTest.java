package com.epam.esm.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.epam.esm.config.RootConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("dev")
@ContextConfiguration(classes = RootConfig.class)
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@WebAppConfiguration
public class SearchGiftCertificateControllerRepositoryIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                                      .build();
    }

    @Test
    void getValidSearch_thenOk() throws Exception {
        String requestBodyJson = """
            {
                "searchText": "e",
                "nameSearchOrder": "ASC",
                "dateSearchOrder": "DESC"
            }""";
        String responseBodyJson = """
            [
                 {
                     "id": 1,
                     "name": "cert1",
                     "description": "description1",
                     "price": 100.00,
                     "duration": 90,
                     "createDate": "2022-12-30T15:07:30.965",
                     "lastUpdateDate": "2022-12-30T15:07:30.965",
                     "tags": [
                         {
                             "id": 1,
                             "name": "tag1"
                         },
                         {
                             "id": 10,
                             "name": "tag10"
                         }
                     ]
                 },
                 {
                     "id": 2,
                     "name": "cert2",
                     "description": "description2",
                     "price": 150.00,
                     "duration": 30,
                     "createDate": "2022-12-30T15:25:05.523",
                     "lastUpdateDate": "2022-12-30T15:25:05.523",
                     "tags": [
                         {
                             "id": 2,
                             "name": "tag2"
                         },
                         {
                             "id": 9,
                             "name": "tag9"
                         }
                     ]
                 },
                 {
                     "id": 3,
                     "name": "cert3",
                     "description": "description3",
                     "price": 200.00,
                     "duration": 60,
                     "createDate": "2023-01-02T18:15:51.505",
                     "lastUpdateDate": "2023-01-02T18:15:51.505",
                     "tags": [
                         {
                             "id": 3,
                             "name": "tag3"
                         },
                         {
                             "id": 8,
                             "name": "tag8"
                         }
                     ]
                 },
                 {
                     "id": 4,
                     "name": "cert4",
                     "description": "description4",
                     "price": 220.50,
                     "duration": 120,
                     "createDate": "2023-01-04T00:20:46.926",
                     "lastUpdateDate": "2023-01-04T00:20:46.926",
                     "tags": [
                         {
                             "id": 4,
                             "name": "tag4"
                         },
                         {
                             "id": 7,
                             "name": "tag7"
                         }
                     ]
                 },
                 {
                     "id": 5,
                     "name": "cert5",
                     "description": "description5",
                     "price": 123.99,
                     "duration": 180,
                     "createDate": "2023-01-04T01:22:25.023",
                     "lastUpdateDate": "2023-01-04T01:22:25.023",
                     "tags": [
                         {
                             "id": 5,
                             "name": "tag5"
                         },
                         {
                             "id": 6,
                             "name": "tag6"
                         }
                     ]
                 }
            ]""";

        mockMvc.perform(get("/api/search").content(requestBodyJson)
                                          .contentType(MediaType.APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(content().json(responseBodyJson));
    }
}
